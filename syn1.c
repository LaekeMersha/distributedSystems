//
//  syn1.c
//  
//
//  Created by Laeke Mersha on 10/26/16.
//
//
#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<semaphore.h>

sem_t *lock;
void display(char *str)
{
    
    sem_wait(lock);
    char *tmp;
    for(tmp=str;*tmp;tmp++)
    {
       
        write(1,tmp,1);
        usleep(100);
        
    }
    sem_post(lock);
   
}
int main()
{
    
    lock = sem_open("lock",O_CREAT,S_IRUSR | S_IWUSR,1);
    int i;
    if (fork())
    {
        for(i=0;i<10;i++)
            display("Hello world\n");
        wait(NULL);
    }
    else
    {
        for (i=0;i<10;i++)
            display("Bonjour monde\n");
    }
    sem_close(lock);
    sem_unlink("lock");
    
    return 0;
}
