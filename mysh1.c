/*
 * mysh1.c
 *
 *  Created on: Oct 28, 2016
 *      Author: Laeke Mersha ATR/2435/06
 */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#define MAXLINE 80

int main(void){
    char arg[MAXLINE];
    int shouldRun = 1;
    while(shouldRun){
        getcwd(arg,sizeof(arg));
        printf("\n%s",arg);
        printf(":mysh1$");
        fflush(stdout);
        scanf(" %[^\n]",arg);
        if((strcmp(arg,"exit")==0)){
            break;
        }
        char *parse = strtok(arg,"\n");
        char *command[] = {parse,NULL};
        pid_t pid = fork();
        if(pid == 0){
            if((execvp(command[0],command)) == -1){
                printf("Error command!!");
                exit(1);
            }
        }
        else if(pid > 0){
            waitpid(pid,0,0);
        }
        else{
            exit(1);
        }
        
        
    }
    return 0;
}
