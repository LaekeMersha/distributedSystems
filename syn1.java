/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package synchronization;

/**
 *
 * @author Apple Laeke Mersha ATR/2435/06
 */
public class syn1 implements Runnable{
    private final String text;
    public syn1(String text)
    {
       this.text = text;
    }
    
    synchronized static void display(String str)
    {
       char[] characters = str.toCharArray();
       for(int i=0; i < characters.length;i++)
          System.out.print(characters[i]);         
    }
    
    public void run()
    {
        for(int i=0; i < 10;i++)
        {
            try {
                display(text);
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public static void main (String[] args)
    {

        Thread thread = new Thread(new syn1("Hello world\n"));
        Thread thread2 = new Thread(new syn1("Bonjour monde\n"));
        thread.start();
        thread2.start();        
    }
}
