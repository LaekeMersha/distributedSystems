/*
 * mysh3.c
 *
 *  Created on: Oct 28, 2016
 *      Author: Laeke Mersha ATR/2435/06
 */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#define MAXLINE 80


void run(char * args[],int piped){// run the command (forking and executing) also if there is pipe
    
    pid_t pid = fork();
    if(pid == 0){
        if(piped != 0)
        {
            int fd[2];
            pipe(fd);
            char *command[MAXLINE/4+1];
            int i = 0;
            pid_t forPipe = fork();
            if(forPipe == 0)
            {
                
                dup2(fd[1],1);
                close(fd[0]);
                
                while (strcmp(args[i],"|") != 0) {
                    command[i] = args[i];
                    i++;
                }
                command[i] = NULL;
                if((execvp(command[0],command)) == -1){
                    printf("Error command!!");
                }
            }
            else if(forPipe > 0)
            {
                
                dup2(fd[0],0);
                close(fd[1]);
                piped ++;
                while (args[piped] != NULL) {
                    command[i] = args[piped];
                    piped++;
                    i++;
                }
                command[i] = NULL;
                if((execvp(command[0],command)) == -1){
                    printf("Error command!!");
                    exit(1);
                }
            }
            else
            {
                printf("Shell: Error!");
                exit(1);
            }
            
        }
        else
        {
            if((execvp(args[0],args)) == -1){
                printf("Error command!!");
                exit(1);
            }
        }
    }
    else if(pid > 0){
        waitpid(pid,0,0);
    }
    else{
        printf("Shell: Error!");
        exit(1);
    }
    
    
}
int parseArgument(char arg[], char *args[]){ // parsing the Args
    char *pch=NULL;
    int count = 0;
    int pipePos= 0;
    pch = strtok(arg," \t\n\f\r");
    args[count] = pch;
    count++;
    while((pch = strtok(NULL," \t\n\f\r")) != NULL){
        if (strcmp("|",pch)==0)
        {
            pipePos = count;
        }
        args[count] = strdup(pch);
        count++;
    }
    args[count] = NULL;
    
    return pipePos;
    
}
int main(void){
    char *args[MAXLINE/2+1];
    char arg[MAXLINE];
    int shouldRun = 1;
    
    while(shouldRun){
        getcwd(arg,sizeof(arg));
        printf("\n%s",arg);
        printf(":mysh3$");
        fflush(stdout);
        scanf(" %[^\n]",arg);
        int piped = parseArgument(arg,args);
        if((strcmp(arg,"exit")==0)){
            shouldRun = 0;
            break;
        }
        if((strcmp(arg,"chgdir")==0)){
            chdir(args[1]);
            continue;
        }
        run(args,piped);
        
    }
    return 0;
}
