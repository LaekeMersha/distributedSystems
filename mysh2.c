/*
 * Shell.c
 *
 *  Created on: Jan 30, 2016
 *      Author: Laeke Mersha ATR/2435/06
 */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#define MAXLINE 80


void parseArgument(char arg[], char *args[]){ // parsing the Args
    char *pch=NULL;
    int count = 0;
    pch = strtok(arg," \t\n\f\r");
    args[count] = pch;
    count++;
    while((pch = strtok(NULL," \t\n\f\r")) != NULL){
        args[count] = strdup(pch);
        count++;
    }
    args[count] = NULL;
}
int main(void){
    char *args[MAXLINE/2+1];
    char arg[MAXLINE];
    int shouldRun = 1;
    
    while(shouldRun){
        getcwd(arg,sizeof(arg));
        printf("\n%s",arg);
        printf(":mysh2$");
        fflush(stdout);
        scanf(" %[^\n]",arg);
        parseArgument(arg,args);
        if((strcmp(arg,"exit")==0)){
            shouldRun = 0;
            break;
        }
        if((strcmp(arg,"chgdir")==0)){
            chdir(args[1]);
            continue;
        }
        pid_t pid = fork();
        if(pid == 0){
            if((execvp(args[0],args)) == -1){
                printf("Error command!!");
                exit(1);
            }
        }
        else if(pid > 0){
            waitpid(pid,0,0);
        }
        else{
            exit(1);
        }

        
    }
    return 0;
}
