//
//  synthread1.c
//  
//
//  Created on Oct 28, 2016.
//  Done by: Laeke Mersha ATR/2435/06
//
//

#include <stdio.h>
#include <unistd.h>
#include<sys/wait.h>
#include<sys/types.h>
#include <pthread.h>
pthread_mutex_t lock;

void display(char *str)
{
    pthread_mutex_lock(&lock);
    char *tmp;
    for(tmp=str;*tmp;tmp++)
    {
        write(1,tmp,1);
        usleep(100);
    }
    pthread_mutex_unlock(&lock);
}
void *loop(void *arg)
{
    char *text = (char *) arg;
    int i;
    for (i = 0; i < 10; i++)
        display(text);
    pthread_exit(NULL);
}
int main(){
    pthread_mutex_init(&lock,NULL);
    pthread_t thread[2];
    pthread_create(&thread[0],NULL,loop,"Hello world\n");
    pthread_create(&thread[1],NULL,loop,"Bonjour monde\n");
   
    pthread_join(thread[0],NULL);
    pthread_join(thread[1],NULL);
    pthread_mutex_destroy(&lock);
    return 0;

}